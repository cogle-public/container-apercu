# apercu-container

This is the Docker project to create a docker image of an apercu survey engine.  It builds off of several components:

```
apercu-build
apercu-studies
apercu-service
```


### Dependencies

| project | description |
| ---| --- |
| apercu-build | contains the front end javascript.  It is build from the apercu-frontend project |
| apercu-studies | these are the studie(s) to insert into the project|
| | see the project for details on how to format a study|
| | (you still need to import to study to start it)
| apercu-service | This is the Django framework to serve up the study |

## To Build the Container:

```
git clone https://gitlab.com/COGLEProject/container-apercu.git
cd container-apercu
git checkout <branch>  (<....> your branch if not master)
docker build --no-cache -t container-apercu .
```

## To run the container locally in Docker (hosted on port 8000):

```
docker run -it -p  8000:8000 --name container-apercu container-apercu
```
Note that the docker run command sets up most of the used service port, but you should always check to see if any more settings are needed, like database port, etc.  Once it is run, it will be in your docker containers list, and you can then continue to run it from there.  
You will have to set port 22 by hand if you want to be able to exec a terminal window. 
View the Dockerfile to note the changes that may need to be made to run the study engine in your environment.

## To push the container image to heroku project cogle-survey:
```
heroku container:login
docker login --username=_ --password=$(heroku auth:token) registry.heroku.com
heroku container:push web -a cogle-survey
heroku container:release web -a cogle-survey
```

The apercu_run.sh file starts up the apercu-service, passing in the $PORT environment variable set by heroku.

## Setting up your service parameters:

In the Dockerfile, the database parameters get set for the service.  They are in the LOGGING_DATABASE environment variable which is fed into the
apercu-service.  You will need to create your own branch of the container and place your specific database url there.  The full list of environment variables to set is:

| environ var | description |
| ---| --- |
| LOGGING_DATABASE | database access string |
| BROWSER_MIN_WIDTH | minimum acceptable browser width (default 2014)|
| BROWSER_MIN_HEIGHT| minimum acceptable browser height (default 800)|
| AUTOLOAD_STUDY | Study uuid to auto load to.  Forces all to be in the specified study |
| UPLOAD_TOKEN | token needed to upload a study |

You will also need to add your own directory to apercu-studies, for your particular study.

---
The container-apercu Docker Container is maintained by Bob Krivacic at the Palo Alto Research Center (PARC, a Xerox company) in California.

## Apercu URL parameters:

Apercu has a couple of parameters that can be included in the URL to control how it works.  One is to turn off user logging & answer reporting, so that users can try out apercu without filling up the database.  

Another 'reset_local' is used to clear out the local browser memory so re-entry starts at the begining of a study.  One should always use logging=off with this option.

| flag | description |  default |
| ---| --- | --- |
| logging | logging=off - turns off user & answer logging |  on |
| reset_local | reset_local=true - will always restart at the beginning of a study.  Use 'logging=off' with this option to keep the database clean. |  false|



