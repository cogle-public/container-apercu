# Dockerfile for Apercu survey study on Alpine Linux
#
# Designed for Research & Development
#
# Copyright (C)2017-2018 PARC, a Xerox company
# Licensed under GPL, Version 3
#
FROM alpine:3.13
MAINTAINER Bob Krivacic <krivacic@parc.com>
#
# 
#########################################################################################

EXPOSE 22
EXPOSE 8000/tcp

RUN apk update && apk add --no-cache \
	openssl \
	openssh \
	bash \
	git \
	curl \
	vim \
	build-base

# install dropbear as the sshd
RUN apk add openssh-sftp-server && \
	sed -i -e 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
	sed -i -e 's/#PermitUserEnvironment no/PermitUserEnvironment yes/' /etc/ssh/sshd_config && \
	mkdir -p /root/.ssh && \
	chmod 700 /root/.ssh && \
	ssh-keygen -A && \
	echo "source /etc/profile.d/color_prompt" > /root/.bashrc && \
	echo "root:root" | chpasswd 

ADD sshd.sh /
RUN chmod +x /sshd.sh
ADD apercu_run.sh /
RUN chmod +x /apercu_run.sh
	
RUN apk add --no-cache python3 python3-dev py-pip\
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && ls -l \
  && pip3 install --upgrade pip

RUN apk update && apk add --no-cache \
	py3-psycopg2 \
	py-yaml

RUN pip3 install greenlet
RUN pip3 install sqlalchemy
RUN pip3 install geopy
RUN pip3 install tmx
RUN pip3 install pathlib
RUN pip3 install parse

RUN pip3 install django django-import-export
RUN pip3 install djangorestframework


WORKDIR "/"
RUN mkdir -p /root/.ssh/
ADD ./id_rsa /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa
RUN ssh-keyscan gitlab.com >> /root/.ssh/known_hosts

# Get the repositories
RUN git clone -b master git@gitlab.com:COGLEProject/apercu-build.git 
RUN git clone -b master git@gitlab.com:COGLEProject/apercu-service.git
RUN git clone -b master git@gitlab.com:COGLEProject/apercu-studies.git


# For Database Service
#ENV LOGGING_DATABASE "postgres://oooqdnmnejwiiw:22e983ffff9d20a7708ba40948b9dee425acf2acf672e508afd81984fb7a9611@ec2-54-83-59-120.compute-1.amazonaws.com:5432/dbo53lunbvaehb"
#ENV LOGGING_DATABASE "postgres://kbfnrlrwvcsiak:80243d973751e657a6bb16880e30bed9132b586020682a1e7529fe6b7930dab0@ec2-54-235-89-123.compute-1.amazonaws.com:5432/de5gql7qbr7btt"
ENV LOGGING_DATABASE "postgres://ucit5kch0jqt0q:p6a788d280c58567bb9302cc0a916dbcb7c5fa81d9d63689f0083817aac0d8e69@ec2-52-4-120-208.compute-1.amazonaws.com:5432/db91phf2mbjgph"
# Install Exigisi
WORKDIR "/apercu-service"
#RUN npm install --save

VOLUME /data
WORKDIR "/"

# How to start-up
CMD ["/apercu_run.sh"]

#CMD ["python"]

# fin.

