#!/bin/ash
. /etc/profile


echo ''

echo ''
echo '   _____                                     '
echo '  /  _  \ ______   ___________   ____  __ __ '
echo ' /  /_\  \\____ \_/ __ \_  __ \_/ ___\|  |  \'
echo '/    |    \  |_> >  ___/|  | \/\  \___|  |  /'
echo '\____|__  /   __/ \___  >__|    \___  >____/ '
echo '        \/|__|        \/            \/       '

cd /apercu-service

echo
echo '-------------------------------------'
echo '         Start service'
echo '-------------------------------------'
echo

#./manage.py makemigrations apercu-service
#./manage.py migrate
./manage.py runserver 0.0.0.0:$PORT



